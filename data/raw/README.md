## RNAseq data
- processed according GTEx guidelines (/psycl/g/mpsziller/liesa/VCF/GTEX_RNASeq/RNASeq.gtex.expression.bed)
- matched with genotype data (79 samples)
- only ensembl gene id kept

## Genotype data
- imputed from Laura (slurmgate)
- preprocessed according PriLer guidelines
- matched with RNAseq (79 samples) 
- matched with SCZ GWAS
- see /psycl/g/mpsziller/lucia/PriLer_PROJECT_inhouseANDlmu/SCRIPTS/run_command.txt 

## Covariates (genotype and RNA)
- Gender and batch (genotype)
- first 3 PCs from genotype
- 15 SVAs from RNAseq

## ATACseq data
- from Michael (samples not matched)
- /psycl/g/mpsziller/Analysis/eQTL/ATAC/All_eQTL_peakSet_hg19.bed coordinates
- /psycl/g/mpsziller/Analysis/eQTL/ATACSeq/ATAC_eQTL_v4__normMat_log2.txt processed peaks

## microRNA data
- from Michael (samples not matched)
- /psycl/g/mpsziller/Analysis/eQTL/microRNA/microAnalysis_0321_diffAnalysis_Norm_all_vst.txt processed expression
- /psycl/g/mpsziller/annotationData/hsa_MTI.txt microRNA targets

## GWAS SCZ res
- from paper 'Common schizophrenia alleles are enriched in mutation-intolerant genes and in regions under strong background selection'
- moved from /psycl/g/mpsziller/Analysis/eQTL/PolyModel/Clozuk2AllRiskGenes.txt

## diff gene expression
- moved from /psycl/g/mpsziller/Analysis/eQTL/PolyModel/eQTL_v5_diffAnalysis_DiffExp_DESeq2_0.5_Ctrl_vs_SCZ01.txt


