#!/bin/bash
#SBATCH --time=7-0
#SBATCH --nodes=1
#SBATCH --mem=80G


module load r_anaconda/4.0.3
export R_LIBS_USER=${HOME}/R/x86_64-pc-linux-gnu-library/4.0/

cd /psycl/g/mpsziller/lucia/geneexpr_decomposition/

Rscript src/clustering_gene_expression_contribution_run.R \
	--fold ./ \
	--type_res $1
