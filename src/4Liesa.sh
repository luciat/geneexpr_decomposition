#!/bin/bash
#SBATCH -o /psycl/g/mpsziller/lucia/geneexpr_decomposition/output/logs/4Liesa.out
#SBATCH -e /psycl/g/mpsziller/lucia/geneexpr_decomposition/output/logs/4Liesa.err
#SBATCH --time=7-0
#SBATCH --nodes=1
#SBATCH --mem=50G


module load r_anaconda/4.0.3
export R_LIBS_USER=${HOME}/R/x86_64-pc-linux-gnu-library/4.0/

cd /psycl/g/mpsziller/lucia/geneexpr_decomposition/

Rscript src/4Liesa_chr21_test.R \
	--fold ./ \
	--ATAC_file ATAC/ATAC_eQTL_v4__normMat_log2.txt \
	--ATAC_pos_file ATAC/All_eQTL_peakSet_hg19.bed \
	--geneRNA_file RNASeq_procGTEX.txt \
	--genotype_file genotype/Genotype_dosage_GSAzlabANDlmu_matchedRNA_ \
	--miRNA_file microRNA/microAnalysis_0321_diffAnalysis_Norm_all_vst_corrNames.txt \
	--miRNA_match_file microRNA/hsa_MTI.txt \
	--cov_file Covariates_PEERfact_PCs.txt \
	--genotype_info_file genotype/Genotype_VariantsInfo_GSAzlabANDlmu-SCZgwas_
