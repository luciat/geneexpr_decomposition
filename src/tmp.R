options(stringsAsFactors=F)
options(max.print=1000)
suppressPackageStartupMessages(library(argparse))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(ggsci))
suppressPackageStartupMessages(library(RColorBrewer))
suppressPackageStartupMessages(library(circlize))
suppressPackageStartupMessages(library(dplyr))
suppressPackageStartupMessages(library(tibble))
suppressPackageStartupMessages(library(RNOmni))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(biomaRt))
suppressPackageStartupMessages(library(Matrix))
suppressPackageStartupMessages(library(glmnet))
suppressPackageStartupMessages(library(ComplexHeatmap))
suppressPackageStartupMessages(library(cluster))
suppressPackageStartupMessages(library(factoextra))
suppressPackageStartupMessages(library(umap))
options(bitmapType = 'cairo', device = 'png')


parser <- ArgumentParser(description="")
parser$add_argument("--ATAC_file", type = "character", help = "")
parser$add_argument("--ATAC_pos_file", type = "character", help = "")
parser$add_argument("--miRNA_file", type = "character", help = "")
parser$add_argument("--miRNA_match_file", type = "character", help = "")
parser$add_argument("--cov_file", type = "character", help = "")
parser$add_argument("--geneRNA_file", type = "character", help = "")
parser$add_argument("--genotype_info_file", type = "character", help = "")
parser$add_argument("--genotype_file", type = "character", help = "")
parser$add_argument("--fold", type="character", help = "")

args <- parser$parse_args()
ATAC_file <- args$ATAC_file
ATAC_pos_file <- args$ATAC_pos_file
miRNA_file <- args$miRNA_file
miRNA_match_file <- args$miRNA_match_file
geneRNA_file <- args$geneRNA_file
cov_file <- args$cov_file
genotype_info_file <- args$genotype_info_file
genotype_file <- args$genotype_file
fold <- args$fold


######################################################################################################
# fold <- '/psycl/g/mpsziller/lucia/geneexpr_decomposition/'
# # infold <- '/psycl/g/mpsziller/lucia/geneexpr_decomposition/data/raw/'
# ATAC_file <- 'ATAC/ATAC_eQTL_v4__normMat_log2.txt'
# ATAC_pos_file <- 'ATAC/All_eQTL_peakSet_hg19.bed'
# geneRNA_file <- 'RNASeq_procGTEX.txt'
# genotype_file <- 'genotype/Genotype_dosage_GSAzlabANDlmu_matchedRNA_'
# miRNA_file <- 'microRNA/microAnalysis_0321_diffAnalysis_Norm_all_vst.txt'
# miRNA_match_file <- 'microRNA/hsa_MTI.txt'
# cov_file <- 'Covariates_PEERfact_PCs.txt'
# genotype_info_file <- 'genotype/Genotype_VariantsInfo_GSAzlabANDlmu-SCZgwas_'
# # outFold <- '/psycl/g/mpsziller/lucia/geneexpr_decomposition/data/processed/'
# # outFold_plot <- '/psycl/g/mpsziller/lucia/geneexpr_decomposition/output/plots/processed/'
# #####################################################################################################

source(sprintf('%s/src/functions.R', fold))

# load cov_file (to be used for annotation)
CovariatesGeneExpGenotype <- fread(sprintf('%s/data/raw/%s',fold, cov_file), h=T, data.table = F)
SampleAnnotation <- CovariatesGeneExpGenotype %>% dplyr::select(Individual_ID, genoSample_ID, RNASample_ID)

# load ATAC and match names
AtacInput <- fread(sprintf('%s/data/raw/%s',fold, ATAC_file), h=T,  data.table = F)
SampleAnnotation$ATACSample_ID <- colnames(AtacInput)[match(SampleAnnotation$Individual_ID, colnames(AtacInput))]
SampleAnnotation$ATACSample_ID[match(c('MC2_1 30.3.2020', 'MC3_2 30.3.2020'),SampleAnnotation$Individual_ID)] <- c('MC2_1.30.3.2020','MC3_2.30.3.2020')
SampleAnnotation$ATACSample_ID[match('SCZ3 Cl3', SampleAnnotation$Individual_ID)] <- 'SCZ3'

# load miRNA and match names
miRNAInput <- read.table(sprintf('%s/data/raw/%s',fold, miRNA_file), h=T)
miRNAInput <- cbind(data.frame(miRNA_ID = rownames(miRNAInput)),miRNAInput)
rownames(miRNAInput) <- NULL
SampleAnnotation$miRNASample_ID <- colnames(miRNAInput)[match(SampleAnnotation$Individual_ID, colnames(miRNAInput))]
SampleAnnotation$miRNASample_ID[match(c('No1', 'SCZ3 Cl3', 'A18945'),SampleAnnotation$Individual_ID)] <- c('NO1','SCZ3', 'HDF6')

# save table
write.table(SampleAnnotation, file = sprintf('%s/data/processed/SamplesID_matchedTables.txt', fold), col.names = T, row.names = F, sep = '\t', quote = F)

# subset with current matching
SampleAnnotation_matched <- SampleAnnotation[rowSums(is.na(SampleAnnotation)) == 0,]
CovariatesGeneExpGenotype_matched <- semi_join(CovariatesGeneExpGenotype, SampleAnnotation_matched) %>% add_column(ATACSample_ID = SampleAnnotation_matched$ATACSample_ID,.before = "Dx") %>% add_column(miRNASample_ID = SampleAnnotation_matched$miRNASample_ID,.before = "Dx")

#######################################
### inverse gaussian transformation ###
#######################################

# miRNA
miRNANormalized <- inverse_transform_gaussian(miRNAInput, 
                                              featuresName_id = 1, save_plot = T, pvalue_thr = 0.01, 
                                              out_file = sprintf('%s/output/plots/processed/microRNA_density', fold))
miRNANormalized_tosave <- cbind(data.frame(id = colnames(miRNANormalized)), as.data.frame(t(miRNANormalized)))
# save
write.table(miRNANormalized_tosave, 
            file = sprintf('%s/data/processed/microRNA_inverseGaussNorm_filtNormPval0.01.txt', fold), 
            col.names = T, row.names = F, sep = '\t', quote = F)

# ATAC
AtacNormalized <- inverse_transform_gaussian(AtacInput, 
                                             featuresName_id = 1:3, save_plot = F, pvalue_thr = 0.01, 
                                             out_file = sprintf('%s/output/plots/processed/ATAC_density', fold))
AtacNormalized_tosave <- cbind(data.frame(id = colnames(AtacNormalized)), as.data.frame(t(AtacNormalized)))
# save
write.table(AtacNormalized_tosave, 
            file = sprintf('%s/data/processed/ATAC_inverseGaussNorm_filtNormPval0.01.txt', fold), 
            col.names = T, row.names = F, sep = '\t', quote = F)

#######################################
### compute PCs from ATAC and miRNA ###
#######################################

PC_miRNA <- extract_PCs(NormalizedData = miRNANormalized, 
                        sampleAdditionalCovariates = CovariatesGeneExpGenotype_matched[, c('miRNASample_ID', 'gender', 'Dx')], 
                        save_plot = T, min_variance_expl = 0.4, 
                        out_file = sprintf('%s/output/plots/processed/miRNA_', fold))

PC_Atac <- extract_PCs(NormalizedData = AtacNormalized, 
                       sampleAdditionalCovariates = CovariatesGeneExpGenotype_matched[, c('ATACSample_ID', 'gender', 'Dx')], 
                       save_plot = T, min_variance_expl = 0.4, 
                       out_file = sprintf('%s/output/plots/processed/Atac_', fold))

# update covariate table and save
colnames(PC_miRNA) <- paste('miRNA', colnames(PC_miRNA), sep = '_')
PC_miRNA <- as.data.frame(PC_miRNA)
PC_miRNA$miRNASample_ID <- rownames(PC_miRNA)

colnames(PC_Atac) <- paste('ATAC', colnames(PC_Atac), sep = '_')
PC_Atac <- as.data.frame(PC_Atac)
PC_Atac$ATACSample_ID <- rownames(PC_Atac)

CovariatesGeneExpGenotype_matched <- CovariatesGeneExpGenotype_matched %>% left_join(PC_Atac) %>% left_join(PC_miRNA)
# save
write.table(CovariatesGeneExpGenotype_matched, file = sprintf('%s/data/input/Covariates_complete_matchedTables.txt', fold), col.names = T, row.names = F, sep = '\t', quote = F)

#########################################
### obtain list of genes from ensembl ###
#########################################

geneAnnotation <- extract_ensemble_gene(GRCh = 37, out_file = sprintf("%s/data/processed/hg19.", fold), 
                                        save_output = T)

geneRNA_Normalized <- read.table(sprintf('%s/data/raw/%s',fold, geneRNA_file), h=T, sep = '\t', check.names = F)
rownames(geneRNA_Normalized) <- geneRNA_Normalized[, 1]
geneRNA_Normalized <- t(geneRNA_Normalized[, -1])
# remove genes that fail to be normal
id_rm <- apply(geneRNA_Normalized, 2, function(x) shapiro.test(x)$p.value) <= 0.01
geneRNA_Normalized <- geneRNA_Normalized[, !id_rm]

# intersect with RNA gene available
geneNames_ensembl <- intersect(geneAnnotation$ensembl_gene_id, colnames(geneRNA_Normalized))
geneAnnotation_current <- geneAnnotation[match(geneNames_ensembl, geneAnnotation$ensembl_gene_id),]
geneRNA_Normalized <- geneRNA_Normalized[,match(geneNames_ensembl, colnames(geneRNA_Normalized))]

###############################
### create gene-SNPs target ###
###############################

SNPGeneDistance <- create_gene_snp_distance_matrix(GenotypeInfo_file = paste0(fold, '/data/raw/',genotype_info_file), 
                                                   geneInfo = geneAnnotation_current,  cisTSSThr = 200000)

# save
save(SNPGeneDistance, file = sprintf('%s/data/input/hg19_SNPGeneDistance.RData', fold))

################################
### create gene-miRNA target ###
################################

miRNAGeneTarget <- create_gene_mirna_target_matrix(miRNA_target_file = paste0(fold, '/data/raw/',miRNA_match_file), 
                                                   miRNANames = colnames(miRNANormalized), 
                                                   geneInfo = geneAnnotation_current)

# save
save(miRNAGeneTarget, file = sprintf('%s/data/input/hg19_miRNAGeneTarget.RData', fold))

###############################
### create gene-ATAC target ###
###############################

ATACGeneDistance <- create_gene_atac_distance_matrix(ATAC_coord_file = paste0(fold, '/data/raw/',ATAC_pos_file), 
                                                     ATACNames = colnames(AtacNormalized), 
                                                     geneInfo = geneAnnotation_current, 
                                                     cisTSSThr = 200000)
# NOTE: removed chrX and Y
# save
save(ATACGeneDistance, file = sprintf('%s/data/input/hg19_ATACGeneDistance.RData', fold))

##########################################################
### annotate each gene with the type of data available ###
##########################################################

idMatchSNPs <- unlist(sapply(SNPGeneDistance$dist, function(x) colSums(x!=0)>1))
idMatchmiRNA <- unlist(sapply(miRNAGeneTarget$dist, function(x) colSums(x!=0)>1))
idMatchATAC <- unlist(sapply(ATACGeneDistance$dist, function(x) colSums(x!=0)>1))
NamesMat <- matrix(data = NA, nrow = nrow(geneAnnotation_current),ncol = 3)
NamesMat[idMatchSNPs,1] <- 'SNP'
NamesMat[idMatchmiRNA,2] <- 'miRNA'
NamesMat[idMatchATAC,3] <- 'ATAC'

geneAnnotation_current$type_data_available <- do.call(paste, c(as.data.frame(NamesMat[, 1:3]), sep = "_"))
# save
write.table(x = geneAnnotation_current, file = sprintf('%s/data/input/hg19_ENSEMBL_gene_biomart.txt', fold), 
            col.names = T, row.names = F, sep = '\t', quote = F)


######################################
### save RNA filtered by normality ###
######################################

geneRNA_Normalized_tosave <- cbind(data.frame(id = colnames(geneRNA_Normalized)), as.data.frame(t(geneRNA_Normalized)))

# save
write.table(geneRNA_Normalized_tosave, 
            file = sprintf('%s/data/processed/geneRNA_inverseGaussNorm_filtNormPval0.01.txt', fold), 
            col.names = T, row.names = F, sep = '\t', quote = F)


#####################################
### run elastic-net for each gene ###
#####################################

sampleAnn <- CovariatesGeneExpGenotype_matched %>% dplyr::select(Individual_ID, genoSample_ID, 
                                                                 RNASample_ID, ATACSample_ID, miRNASample_ID, Dx)
covDat <- CovariatesGeneExpGenotype_matched %>% dplyr::select(!(Individual_ID:Dx))

curmiRNA <- miRNANormalized_tosave[match(miRNAGeneTarget$mirna, miRNANormalized_tosave$id), ]
curmiRNA <- curmiRNA[, match(sampleAnn$miRNASample_ID, colnames(miRNANormalized_tosave))]


# stratify samples based on Dx
set.seed(9)
CVfold <- generateCVRuns(labels = sampleAnn$Dx, ntimes = 1, nfold = 5, stratified = T)[[1]]
CVfold_id <- vector(mode = 'numeric', length = nrow(sampleAnn))
for(fold_id in 1:length(CVfold)){
  CVfold_id[CVfold[[fold_id]]] <- fold_id
}

# only include genes SNP_miRNA_ATAC
gene_to_test <- geneAnnotation_current[geneAnnotation_current$type_data_available == 'SNP_miRNA_ATAC', ]

# divide by chromosome (load genotype table one at a time)
output_version1_to4 <- list()
for(chr_id in 1:22){
  
  curChrom <- paste0('chr', chr_id)
  print(paste0('#################', curChrom, '#################'))
  
  genes <- gene_to_test[gene_to_test$chromosome == curChrom,]
  
  # SNP
  curGeno <- read.table(gzfile(sprintf('%s/data/raw/%s%s_matrix.txt.gz', fold, genotype_file, curChrom)), header = T, sep = '\t', check.names=F)
  curGeno <- curGeno[, match(sampleAnn$genoSample_ID, colnames(curGeno))]
  rownames(curGeno) <- paste(SNPGeneDistance$snp[[chr_id]]$chrom, SNPGeneDistance$snp[[chr_id]]$position, SNPGeneDistance$snp[[chr_id]]$ID, sep = '_')
  
  # RNA
  curRNA <- geneRNA_Normalized_tosave[match(genes$ensembl_gene_id,geneRNA_Normalized_tosave$id),]
  curRNA <- curRNA[, match(sampleAnn$RNASample_ID, colnames(curRNA))]
  
  # ATAC 
  curATAC <- AtacNormalized_tosave[match(ATACGeneDistance$atac[[chr_id]]$id, AtacNormalized_tosave$id), ]
  curATAC <- curATAC[, match(sampleAnn$ATACSample_ID, colnames(curATAC))]
  
  devRatio <- matrix(NA, nrow = nrow(genes), ncol = 6)
  corrModel <- matrix(NA, nrow = nrow(genes), ncol = 6)
  colnames(devRatio) <- colnames(corrModel) <- c('Total', 'Geno', 'miRNA', 'ATAC', 'Geno_miRNA_ATAC','Cov')
  
  devRatio_covFixed <- matrix(NA, nrow = nrow(genes), ncol = 6)
  corrModel_covFixed <- matrix(NA, nrow = nrow(genes), ncol = 6)
  colnames(devRatio_covFixed) <- colnames(corrModel_covFixed) <- c('Total', 'Geno', 'miRNA', 'ATAC',  'Geno_miRNA_ATAC', 'Cov')
  
  devRatio_covRegressOut <- matrix(NA, nrow = nrow(genes), ncol = 4)
  corrModel_covRegressOut <- matrix(NA, nrow = nrow(genes), ncol = 4)
  colnames(devRatio_covRegressOut) <- colnames(corrModel_covRegressOut) <- c('Geno_miRNA_ATAC', 'Geno', 'miRNA', 'ATAC')
  
  corrModel_decompose <- matrix(NA, nrow = nrow(genes), ncol = 6)
  colnames(corrModel_decompose) <- c('Total', 'Geno', 'miRNA', 'ATAC', 'Geno_miRNA_ATAC','Cov')
  
  for(id_gene in 1:nrow(genes)){
    
    print(id_gene)
    geneName <- genes$ensembl_gene_id[id_gene]
    
    id_snps <- get_notzero_features_for_gene(geneName, SNPGeneDistance, chr_id)
    id_atac <- get_notzero_features_for_gene(geneName, ATACGeneDistance, chr_id)
    id_mirna <- get_notzero_features_for_gene(geneName, miRNAGeneTarget, chr_id)
    
    y <- t(curRNA[id_gene, ,drop=F])
    x_geno <- t(curGeno[id_snps,,drop=F])
    x_atac <- t(curATAC[id_atac,,drop=F])
    x_mirna <- t(curmiRNA[id_mirna,,drop=F])
    x_cov <- as.matrix(covDat)
    
    
    #### VERSION 1, ANOVA STRATEGY ####
    ## full model
    TotalRes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_atac, x_mirna, x_cov), foldid = CVfold_id)
    
    ## model without Genotype
    MinusGenoRes <- elastic_net_optLambda(y = y, X = cbind(x_atac, x_mirna, x_cov), foldid = CVfold_id)
    
    ## model without miRNA
    MinusmiRNARes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_atac, x_cov), foldid = CVfold_id)
    
    ## model without ATAC
    MinusATACRes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_mirna, x_cov), foldid = CVfold_id)
    
    ## model without covariates
    MinusCovRes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_atac, x_mirna), foldid = CVfold_id)
    
    ## model only covariates
    OnlyCovRes <- elastic_net_optLambda(y = y, X = x_cov, foldid = CVfold_id)
    
    devRatio[id_gene,] <- c(TotalRes$R2, TotalRes$R2 - MinusGenoRes$R2, 
                            TotalRes$R2 - MinusmiRNARes$R2, 
                            TotalRes$R2 - MinusATACRes$R2,
                            TotalRes$R2 - OnlyCovRes$R2,
                            TotalRes$R2 - MinusCovRes$R2)
    
    corrModel[id_gene,] <- c(TotalRes$cor, TotalRes$cor - MinusGenoRes$cor, 
                             TotalRes$cor - MinusmiRNARes$cor, 
                             TotalRes$cor -  MinusATACRes$cor, 
                             TotalRes$cor - OnlyCovRes$cor,
                             TotalRes$cor - MinusCovRes$cor)
    
    #### VERSION 2, ANOVA STRATEGY force COV not zero ####
    ## full model
    TotalRes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_atac, x_mirna, x_cov), foldid = CVfold_id,
                                      penalty_coeff = c(rep(1, ncol(x_geno) + ncol(x_atac) + ncol(x_mirna)), rep(0, ncol(x_cov))))
    
    ## model without Genotype
    MinusGenoRes <- elastic_net_optLambda(y = y, X = cbind(x_atac, x_mirna, x_cov), foldid = CVfold_id,
                                          penalty_coeff = c(rep(1,ncol(x_atac) + ncol(x_mirna)), rep(0, ncol(x_cov))))
    
    ## model without miRNA
    MinusmiRNARes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_atac, x_cov), foldid = CVfold_id,
                                           penalty_coeff = c(rep(1, ncol(x_geno) + ncol(x_atac)), rep(0, ncol(x_cov))))
    
    ## model without ATAC
    MinusATACRes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_mirna, x_cov), foldid = CVfold_id,
                                          penalty_coeff = c(rep(1,ncol(x_geno) + ncol(x_mirna)), rep(0, ncol(x_cov))))
    
    ## model without covariates
    MinusCovRes <- elastic_net_optLambda(y = y, X = cbind(x_geno, x_atac, x_mirna), foldid = CVfold_id)
    
    ## model only covariates
    OnlyCovRes <- elastic_net_optLambda(y = y, X = x_cov, foldid = CVfold_id)
    
    devRatio_covFixed[id_gene,] <- c(TotalRes$R2, TotalRes$R2 - MinusGenoRes$R2, 
                                     TotalRes$R2 - MinusmiRNARes$R2, 
                                     TotalRes$R2 - MinusATACRes$R2,
                                     TotalRes$R2 - OnlyCovRes$R2,
                                     TotalRes$R2 - MinusCovRes$R2)
    
    corrModel_covFixed[id_gene,] <- c(TotalRes$cor, TotalRes$cor - MinusGenoRes$cor, 
                                      TotalRes$cor - MinusmiRNARes$cor, 
                                      TotalRes$cor -  MinusATACRes$cor, 
                                      TotalRes$cor - OnlyCovRes$cor,
                                      TotalRes$cor - MinusCovRes$cor)
    
    
    #### VERSION 3, regress out covariates initially ####
    
    yAndCov <- cbind(data.frame(y= y[,1]), x_cov)
    y_regressOut_cov <- resid(lm(formula = y~., data = yAndCov))
    y_regressOut_cov <- t(t(y_regressOut_cov))
    
    ## full model
    TotalRes <- elastic_net_optLambda(y = y_regressOut_cov, X = cbind(x_geno, x_atac, x_mirna), foldid = CVfold_id)
    
    ## model only Genotype
    GenoRes <- elastic_net_optLambda(y = y_regressOut_cov, X = x_geno, foldid = CVfold_id)
    
    ## model only miRNA
    miRNARes <- elastic_net_optLambda(y = y_regressOut_cov, X = x_mirna, foldid = CVfold_id)
    
    ## model only ATAC
    ATACRes <- elastic_net_optLambda(y = y_regressOut_cov, X = x_atac, foldid = CVfold_id)
    
    devRatio_covRegressOut[id_gene,] <- c(TotalRes$R2, GenoRes$R2, miRNARes$R2, ATACRes$R2)
    corrModel_covRegressOut[id_gene,] <- c(TotalRes$cor, GenoRes$cor, miRNARes$cor, ATACRes$cor)
    
    
    #### VERSION 4, force COV not zero, compute correlation based on single prediction of the total model ####
    
    ResDecompCorr <- elastic_net_optLambda_decomposeCorr(y = y, X = cbind(x_geno,x_mirna, x_atac, x_cov), 
                                                         id_data_type = list(geno = 1:ncol(x_geno), 
                                                                             mirna = ncol(x_geno) + (1:ncol(x_mirna)), 
                                                                             atac = ncol(x_geno) + ncol(x_mirna) + (1:ncol(x_atac)), 
                                                                             not_cov = 1:(ncol(x_geno) + ncol(x_atac) + ncol(x_mirna)),
                                                                             cov = ncol(x_geno) + ncol(x_atac) + ncol(x_mirna) + (1:ncol(x_cov))), 
                                                         foldid = CVfold_id,
                                                         penalty_coeff = c(rep(1, ncol(x_geno) + ncol(x_atac) + ncol(x_mirna)), rep(0, ncol(x_cov))))
    
    corrModel_decompose[id_gene, ] <- ResDecompCorr$cor$est
    
  }
  
  output_version1_to4[[chr_id]] <- list(v1_devratio = devRatio, v1_corr = corrModel, 
                                        v2_devratio = devRatio_covFixed, v2_corr = corrModel_covFixed, 
                                        v3_devratio = devRatio_covRegressOut, v3_corr = corrModel_covRegressOut, 
                                        v4_corr = corrModel_decompose)
  
  
}

output_annGenes <- list(genes = gene_to_test, model_performance = output_version1_to4)

save(output_annGenes, file = sprintf('%s/output/Genes_withSNPmiRNAATAC_modelPerformance.RData', fold))

######################
#### plot results ####
######################

names_results <- names(output_annGenes[[2]][[1]])
output_allchr <- vector(mode = 'list', length = length(names_results))
names(output_allchr) <- names_results

# combine for each method
for(type_res in names_results){
  output_allchr[[type_res]] <- do.call(rbind, lapply(output_annGenes[[2]], function(x) x[[type_res]]))
  output_allchr[[type_res]] <- cbind(output_annGenes[[1]], as.data.frame(output_allchr[[type_res]]))
}

typeResult_plot <- c('devRatio (version 1)', 'Correlation (version 1)', 
                     'devRatio (version 2)', 'Correlation (version 2)',
                     'devRatio (version 3)', 'Correlation (version 3)',
                     'Correlation (version 4)')

for(id_type_res in 1:length(names_results)){
  print(id_type_res)
  type_res <- names_results[id_type_res]
  plot_estimation(outputDF=output_allchr[[type_res]], typeResult = typeResult_plot[id_type_res], 
                  save_file = sprintf('%s/output/plots/%s_', fold, type_res))  
}

#########################
#### save v1 results ####
#########################

write.table(x = output_allchr[[1]], 
            file = sprintf('%s/output/Genes_withSNPmiRNAATAC_modelPerformance_v1R2.txt', fold), 
            col.names = T, row.names = F, sep = '\t', quote = F)

write.table(x = output_allchr[[2]], 
            file = sprintf('%s/output/Genes_withSNPmiRNAATAC_modelPerformance_v1corr.txt', fold), 
            col.names = T, row.names = F, sep = '\t', quote = F)

################################
#### clustering based on R2 ####
################################

corr_modelPreformance <- read.table(sprintf('%s/output/Genes_withSNPmiRNAATAC_modelPerformance_v1corr.txt', fold), h=T, stringsAsFactors=F, sep = '\t')
rownames(corr_modelPreformance) <- corr_modelPreformance$external_gene_name

R2_modelPreformance <- read.table(sprintf('%s/output/Genes_withSNPmiRNAATAC_modelPerformance_v1R2.txt', fold), h=T, stringsAsFactors=F, sep = '\t')
rownames(R2_modelPreformance) <- R2_modelPreformance$external_gene_name

inputData <- R2_modelPreformance[,c('Geno', 'miRNA', 'ATAC')]

# # Plot the obtained dendrogram
# png(sprintf('%s/output/plots/heatmap.png', fold), width = 5, height = 10, units = "in", res = 100)
# ht <- Heatmap(inputData, cluster_columns=F, 
#               col = circlize::colorRamp2(c(-1,0,1), c("blue", "white", "red")), 
#               heatmap_legend_param = list(title = "R2"))
# draw(ht)
# dev.off()
# 
# custom.settings <- umap.defaults
# custom.settings$n_neighbors <- 30
# custom.settings$min_dist <- 0.01
# umapR2 <- umap(inputData, config = custom.settings)
# df_umap_R2 <- cbind(data.frame(UMAP1 = umapR2$layout[,1], UMAP2 = umapR2$layout[,2]), inputData)
# 
# pl1 <- ggplot(data = df_umap_R2, aes(x = UMAP1, y = UMAP2, color = Geno))+
#   geom_point(size = 0.5, alpha = 0.7)+
#   theme_bw()+
#   scale_color_gradient2(high = "red", mid = "grey", low = "blue")
# pl2 <- ggplot(data = df_umap_R2, aes(x = UMAP1, y = UMAP2, color = miRNA))+
#   geom_point(size = 0.5, alpha = 0.7)+
#   theme_bw()+
#   scale_color_gradient2(high = "red", mid = "grey", low = "blue")
# pl3 <- ggplot(data = df_umap_R2, aes(x = UMAP1, y = UMAP2, color = ATAC))+
#   geom_point(size = 0.5, alpha = 0.7)+
#   theme_bw()+
#   scale_color_gradient2(high = "red", mid = "grey", low = "blue")
# 
# 
# tot_pl <- ggarrange(plotlist = list(pl1, pl2, pl3), nrow = 1, align='h')
# ggsave(plot = tot_pl, filename = sprintf('%s/output/plots/umap.png', fold),width = 15, height = 5, dpi = 200)
# 
# 
# pl <- fviz_nbclust(inputData, pam, method = "silhouette")+ theme_classic()
# ggsave(plot = pl, filename = sprintf('%s/output/plots/pam_clust_silh.png', fold),width = 5, height = 5, dpi = 200)

#### rescale 
thr_rescale <- apply(inputData, 2, function(x) quantile(x, probs = c(0.01, 0.99)))
new_input <- inputData
for(col in 1:ncol(inputData)){
  new_input[new_input[, col] <= thr_rescale[1, col],col] <- thr_rescale[1, col]
  new_input[new_input[, col] >= thr_rescale[2, col],col] <- thr_rescale[2, col]
}

new_input <- scale(new_input)

# Plot the obtained dendrogram
png(sprintf('%s/output/plots/heatmap_rescale.png', fold), width = 5, height = 10, units = "in", res = 100)
ht <- Heatmap(new_input, cluster_columns=F, 
              col = circlize::colorRamp2(c(min(new_input),0,max(new_input)), c("blue", "white", "red")), 
              heatmap_legend_param = list(title = "Z-score R2"))
draw(ht)
dev.off()

umapR2 <- umap(new_input, config = custom.settings)
df_umap_R2 <- cbind(data.frame(UMAP1 = umapR2$layout[,1], UMAP2 = umapR2$layout[,2]), new_input)

pl1 <- ggplot(data = df_umap_R2, aes(x = UMAP1, y = UMAP2, color = Geno))+
  geom_point(size = 0.5, alpha = 0.7)+
  theme_bw()+
  scale_color_gradient2(high = "red", mid = "grey", low = "blue")
pl2 <- ggplot(data = df_umap_R2, aes(x = UMAP1, y = UMAP2, color = miRNA))+
  geom_point(size = 0.5, alpha = 0.7)+
  theme_bw()+
  scale_color_gradient2(high = "red", mid = "grey", low = "blue")
pl3 <- ggplot(data = df_umap_R2, aes(x = UMAP1, y = UMAP2, color = ATAC))+
  geom_point(size = 0.5, alpha = 0.7)+
  theme_bw()+
  scale_color_gradient2(high = "red", mid = "grey", low = "blue")

tot_pl <- ggarrange(plotlist = list(pl1, pl2, pl3), nrow = 1, align='h')
ggsave(plot = tot_pl, filename = sprintf('%s/output/plots/umap_rescale.png', fold),width = 15, height = 5, dpi = 200)


pl1 <- fviz_nbclust(new_input, pam, method = "silhouette")+ theme_classic()
ggsave(plot = pl1, filename = sprintf('%s/output/plots/pam_clust_silh_rescale.png', fold),width = 5, height = 5, dpi = 200)

cl <- pam(new_input, k = 6)
df <- data.frame(cl = cl$clustering, R2 = unlist(inputData), 
                 type = unlist(lapply(colnames(inputData), function(x) rep(x, nrow(inputData)))))
df$cl <- factor(df$cl)
df$type <- factor(df$type, levels = c('Geno', 'miRNA', 'ATAC'))

pl_box <- ggplot(data = df, aes(x = cl, y = R2, fill = type))+
  geom_boxplot(alpha = 0.7)+
  theme_bw()
ggsave(plot = pl_box, filename = sprintf('%s/output/plots/boxplot_cl_rescale.png', fold),width = 15, height = 5, dpi = 200)

df_umap_R2 <- data.frame(UMAP1 = umapR2$layout[,1], UMAP2 = umapR2$layout[,2], cl = cl$clustering)
df_umap_R2$cl <- factor(df_umap_R2$cl)
pl <- ggplot(data = df_umap_R2, aes(x = UMAP1, y = UMAP2, color = cl))+
  geom_point(size = 0.5, alpha = 0.7)+
  theme_bw()
ggsave(plot = pl, filename = sprintf('%s/output/plots/umap_rescale_cl.png', fold),width = 5, height = 5, dpi = 200)

# to save
output_tosave <- list(scaled_R2 = new_input, original_R2 = R2_modelPreformance[,-(1:9)], 
                      original_corr = corr_modelPreformance[,-(1:9)], 
                      cl = cl$clustering, geneAnn = corr_modelPreformance[, 1:9])

save(output_tosave, file = sprintf('%s/output/Clustering_Genes_R2based_v1.RData', fold))

