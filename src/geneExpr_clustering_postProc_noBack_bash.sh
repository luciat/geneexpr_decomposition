#!/bin/bash
#SBATCH --time=7-0
#SBATCH --nodes=1
#SBATCH --mem=80G


module load r_anaconda/4.0.3
export R_LIBS_USER=${HOME}/R/x86_64-pc-linux-gnu-library/4.0/

cd /psycl/g/mpsziller/lucia/geneexpr_decomposition/

Rscript src/clustering_postProc_analysis_run.R \
	--fold ./ \
	--type_res $1 \
	--gwas_file data/raw/GWAS_SCZ_Clozuk2AllRiskGenes.txt \
	--diffExp_file data/raw/eQTL_v5_diffAnalysis_DiffExp_DESeq2_0.5_Ctrl_vs_SCZ01.txt
