options(stringsAsFactors=F)
options(max.print=1000)
suppressPackageStartupMessages(library(argparse))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(ggsci))
suppressPackageStartupMessages(library(RColorBrewer))
suppressPackageStartupMessages(library(circlize))
suppressPackageStartupMessages(library(dplyr))
suppressPackageStartupMessages(library(tibble))
suppressPackageStartupMessages(library(ComplexHeatmap))
suppressPackageStartupMessages(library(cluster))
suppressPackageStartupMessages(library(clusterProfiler))
suppressPackageStartupMessages(library(org.Hs.eg.db))
suppressPackageStartupMessages(library(ReactomePA))
options(bitmapType = 'cairo', device = 'png')

parser <- ArgumentParser(description="")
parser$add_argument("--fold", type = "character", help = "")
parser$add_argument("--type_res", type = "character", help = "")
parser$add_argument("--gwas_file", type = "character", help = "")
parser$add_argument("--diffExp_file", type = "character", help = "")
parser$add_argument("--backgroudGenes_file", type = "character", default = NULL, help = "")

# fold <- '/psycl/g/mpsziller/lucia/geneexpr_decomposition/'
args <- parser$parse_args()
fold <- args$fold
type_res <- args$type_res
gwas_file <- args$gwas_file
diffExp_file <- args$diffExp_file
backgroudGenes_file <- args$backgroudGenes_file

##############################
# fold <- '/psycl/g/mpsziller/lucia/geneexpr_decomposition/'
# gwas_file <- 'data/raw/GWAS_SCZ_Clozuk2AllRiskGenes.txt'
# type_res <- 'v2_devratio'
# diffExp_file <- 'data/raw/eQTL_v5_diffAnalysis_DiffExp_DESeq2_0.5_Ctrl_vs_SCZ01.txt'
# backgroudGenes_file <-  'data/processed/hg19.ENSEMBL_gene_biomart.txt'
##############################

#################
### functions ###
#################

fisher_test_pergroup <- function(backGround_genes, cluster_df, list_genes_2, id_gene = 'ensembl_gene_id'){
  
  n_cl <- length(unique(cluster_df$cl))
  fisherTest <- data.frame(matrix(vector(), n_cl, 7,
                                  dimnames=list(c(), c("cluster", "p_value", "odds_ratio", "common_set", "n_shared", "n_clust", "n_list"))),
                           stringsAsFactors=F)
  fisherTest$cluster <- 1:n_cl
  
  for(id_cl in 1:n_cl){
    names_genes_cluster <- cluster_df[cluster_df$cl == id_cl, id_gene]
    
    binary_cluster <- rep(0, length(backGround_genes))
    binary_cluster[backGround_genes %in% names_genes_cluster] <- 1
    
    binary_list <- rep(0, length(backGround_genes))
    binary_list[backGround_genes %in% list_genes_2] <- 1
    
    fisherTest$p_value[id_cl] <- fisher.test(binary_cluster, binary_list, alternative = 'greater')$p.value
    fisherTest$odds_ratio[id_cl] <- fisher.test(binary_cluster, binary_list, alternative = 'greater')$estimate
    fisherTest$common_set[id_cl] <- paste0(backGround_genes[binary_list == 1 & binary_cluster == 1], collapse = '/')
    fisherTest$n_shared[id_cl] <- sum(binary_list == 1 & binary_cluster == 1)
    fisherTest$n_clust[id_cl] <- sum(binary_cluster == 1)
    fisherTest$n_list[id_cl] <- sum(binary_list == 1)
    
      
  }

  return(fisherTest)
}



##############################
# load cluster
clusterObject <- get(load(sprintf('%s/output/Clustering_Genes_combined_%s.RData', fold, type_res)))
# load additional tables
diffExp <- read.table(sprintf('%s%s', fold, diffExp_file), h=T, stringsAsFactors = F, sep = '\t')
gwasHits <- read.table(sprintf('%s%s', fold, gwas_file), h=T, stringsAsFactors = F, sep = '\t', check.names = F)

extract_1 <- clusterObject$geneAnn_SNPmiRNAATAC %>% dplyr::select(ensembl_gene_id, external_gene_name, chromosome, start_position) %>% add_column(type = "SNPmiRNAATAC")
extract_2 <- clusterObject$geneAnn_SNPATAC %>% dplyr::select(ensembl_gene_id, external_gene_name, chromosome, start_position) %>% add_column(type = "SNPATAC")
GenesCluster <- rbind(extract_1, extract_2)
GenesCluster$cl <- clusterObject$cl[match(GenesCluster$ensembl_gene_id,names(clusterObject$cl))]
  
if(is.null(backgroudGenes_file)){
  backgroudGenes_tot <- GenesCluster
}else{
  backgroudGenes_tot <- read.table(sprintf('%s%s', fold, backgroudGenes_file), header = T, stringsAsFactors = F)
  backgroudGenes_tot <- backgroudGenes_tot %>% filter(chromosome %in% paste0('chr', 1:22))
}


##############################
#### enrichment with GWAS ####
##############################

gwasGenes <- unlist(sapply(gwasHits[, 'Gene(s) tagged'], function(x) strsplit(x = x, split = ',')[[1]]))
gwasGenes <- unique(gsub(" ", "", gwasGenes, fixed = TRUE))
# for backgroud remove genes in MHC

backGround_genes <- backgroudGenes_tot %>% filter(!(chromosome == 'chr6' & start_position >= 24000000 & start_position <= 36000000)) %>% dplyr::select(external_gene_name)
backGround_genes <- backGround_genes$external_gene_name

enrichmentGwas <- fisher_test_pergroup(backGround_genes = backGround_genes, 
                                               cluster_df = GenesCluster, list_genes_2 = gwasGenes, 
                                               id_gene = 'external_gene_name')
print(enrichmentGwas)


#################################
#### enrichment with diffExp ####
#################################

diffExpGenes <- diffExp$ensembl_gene_id
backGround_genes <- backgroudGenes_tot$ensembl_gene_id

enrichmentDiff <- fisher_test_pergroup(backGround_genes = backGround_genes, 
                                               cluster_df = GenesCluster, list_genes_2 = diffExpGenes)

print(enrichmentDiff)

############################
#### pathway enrichment ####
############################

goEnrich_output <- list()
ReactomeEnrich_output <- list()

for(id_cl in 1:length(unique(GenesCluster$cl))){
  print(id_cl)
  
  names_genes_cluster <- GenesCluster[GenesCluster$cl == id_cl, 'ensembl_gene_id']
  En2Entrez_cl <- clusterProfiler::bitr(geneID = names_genes_cluster, fromType = "ENSEMBL", toType = "ENTREZID", OrgDb = org.Hs.eg.db)
  En2Entrez_tot <- clusterProfiler::bitr(geneID = backgroudGenes_tot$ensembl_gene_id, fromType = "ENSEMBL", toType = "ENTREZID", OrgDb = org.Hs.eg.db)
  
  goEnrich_output[[id_cl]] <- list(MF = NULL, BP = NULL, CC = NULL)
  
  goEnrich_output[[id_cl]]$MF <- enrichGO(gene          = names_genes_cluster,
                                       universe      = backgroudGenes_tot$ensembl_gene_id, 
                                       OrgDb         = org.Hs.eg.db,
                                       keyType       = "ENSEMBL",
                                       ont           = "MF",
                                       pAdjustMethod = "none",
                                       pvalueCutoff  = 0.01,
                                       qvalueCutoff  = 1,
                                       readable      = TRUE)
  
  goEnrich_output[[id_cl]]$BP <- enrichGO(gene          = names_genes_cluster,
                                          universe      = backgroudGenes_tot$ensembl_gene_id, 
                                          OrgDb         = org.Hs.eg.db,
                                          keyType       = "ENSEMBL",
                                          ont           = "BP",
                                          pAdjustMethod = "none",
                                          pvalueCutoff  = 0.01,
                                          qvalueCutoff  = 1,
                                          readable      = TRUE)
  
  goEnrich_output[[id_cl]]$CC <- enrichGO(gene          = names_genes_cluster,
                                          universe      = backgroudGenes_tot$ensembl_gene_id, 
                                          OrgDb         = org.Hs.eg.db,
                                          keyType       = "ENSEMBL",
                                          ont           = "CC",
                                          pAdjustMethod = "none",
                                          pvalueCutoff  = 0.01,
                                          qvalueCutoff  = 1,
                                          readable      = TRUE)
  
  
  ReactomeEnrich_output[[id_cl]] <- enrichPathway(gene = En2Entrez_cl$ENTREZID,
                                                  organism = "human",
                                                  pvalueCutoff = 0.01,
                                                  pAdjustMethod = "none",
                                                  qvalueCutoff = 1,
                                                  universe = En2Entrez_tot$ENTREZID,
                                                  readable = TRUE)
}

# save output
output_enrichment <- list(GeneCluster = GenesCluster, GWAS = enrichmentGwas, DiffExp = enrichmentDiff, GOPathway = goEnrich_output, 
                          ReactomePathway = ReactomeEnrich_output)

file_name <- sprintf('%s/output/enrichment_cluster_genes_%s.RData', fold, type_res)
if(!is.null(backgroudGenes_file)){
  file_name <- sprintf('%s/output/enrichment_cluster_genes_extBack_%s.RData', fold, type_res)
}

save(output_enrichment, file = file_name)

