Decompose Gene expression in genetic, microRNA and ATAC peaks contribution 
68 matching samples from LMU-MPI

Main script: src/gene_expression_decomposition_run.R
Model evaluation output: output/Genes_withSNPmiRNAATAC_modelPerformance.RData
